#!/home/hz/.rbenv/shims/ruby

module Enumerable
  def my_each
    len = self.length
    len.times do |x|
      yield(self[x])
    end
    return self
  end
end

['2','3','6','4'].my_each do |x|
  puts "x: #{x}, Doubled: " + (x.to_i * 2).to_s
end
